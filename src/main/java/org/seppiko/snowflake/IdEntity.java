/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.snowflake;

/**
 * Snowflake ID entity
 *
 * @author Leonard Woo
 */
public class IdEntity {

  private final long timestamp;
  private final long datacenterId;
  private final long workerId;
  private final long sequence;

  /**
   * Snowflake ID entity constructor.
   *
   * @param timestamp ID timestamp.
   * @param datacenterId data center ID.
   * @param workerId machine or process ID.
   * @param sequence sequence number.
   */
  protected IdEntity(long timestamp, long datacenterId, long workerId, long sequence) {
    this.timestamp = timestamp;
    this.datacenterId = datacenterId;
    this.workerId = workerId;
    this.sequence = sequence;
  }

  /**
   * Get Snowflake id timestamp or offset timestamp.
   *
   * @return Timestamp (millisecond).
   */
  public long getTimestamp() {
    return timestamp;
  }

  /**
   * @return Data center number the process running on.
   */
  public long getDatacenterId() {
    return datacenterId;
  }

  /**
   * @return Machine or process number.
   */
  public long getWorkerId() {
    return workerId;
  }

  /**
   * @return Sequence number.
   */
  public long getSequence() {
    return sequence;
  }

  /**
   * @return Snowflake id entity string.
   */
  @Override
  public String toString() {
    return "Id {" +
        "timestamp=" + timestamp +
        ", datacenterId=" + datacenterId +
        ", workerId=" + workerId +
        ", sequence=" + sequence +
        "}";
  }
}
