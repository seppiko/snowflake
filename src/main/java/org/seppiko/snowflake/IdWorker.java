/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.snowflake;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Seppiko Snowflake algorithm implement.
 *
 * <pre>
 * +----------------------------------------------------------------------------------------------------------------------+
 * | UNUSED(1BIT) |           TIMESTAMP(41BIT)           |  DATACENTER-ID(5BIT)  |  WORKER-ID(5BIT)  |  SERIAL-NO(12BIT)  |
 * +----------------------------------------------------------------------------------------------------------------------+
 * </pre>
 *
 * @see <a href="https://github.com/twitter/snowflake">Snowflake</a>
 * @author Leonard Woo
 */
public final class IdWorker {

  // unused length
  private final long unusedBits = 1L;
  // Datetime length
  private final long timestampBits = 41L;
  // Data identifier id length
  private final long datacenterIdBits = 5L;
  // Worker id length
  private final long workerIdBits = 5L;
  // Sequence length
  private final long sequenceBits = 12L;

  // max values of timeStamp, workerId, datacenterId and sequence
  private final long maxDatacenterId = ~(-1L << datacenterIdBits); // 2^5-1
  private final long maxWorkerId = ~(-1L << workerIdBits); // 2^5-1
  private final long maxSequence = ~(-1L << sequenceBits); // 2^12-1

  // left shift bits of timeStamp, workerId and datacenterId
  private final long timestampShift = sequenceBits + datacenterIdBits + workerIdBits;
  private final long datacenterIdShift = sequenceBits + workerIdBits;
  private final long workerIdShift = sequenceBits;

  // offset timestamp
  private long offsetTimestamp;

  /**
   * data center number the process running on, its value can't be modified
   * after initialization.
   */
  private long datacenterId = -1;

  /**
   * machine or process number, its value can't be modified after
   * initialization.
   */
  private long workerId = -1;

  /**
   * the unique and incrementing sequence number scoped in only one
   * period/unit (here is ONE millisecond). its value will be increased by 1
   * in the same specified period and then reset to 0 for next period.
   */
  private long sequence = 0L;

  /** The time stamp last snowflake ID generated */
  private long lastTimestamp = -1L;

  /** Singleton instance */
  private static IdWorker INSTANCE;

  /**
   * Singleton with synchronized.
   *
   * @param offsetTimestamp Initialize offset timestamp (millisecond).
   * @return Synchronized instance.
   */
  public static synchronized IdWorker getInstance(long offsetTimestamp) {
    if (INSTANCE == null) {
      INSTANCE = new IdWorker(offsetTimestamp);
    }
    return INSTANCE;
  }

  /**
   * IdWorker Constructor.
   *
   * @param offsetTimestamp Initialize offset timestamp (millisecond).
   */
  private IdWorker(long offsetTimestamp) {
    this.offsetTimestamp = offsetTimestamp;
  }

  /**
   * Initialization worker.
   *
   * @param datacenterId See {@link #setDatacenterId}.
   * @param workerId See {@link #setWorkerId}.
   */
  public void initWorker(long datacenterId, long workerId) {
    setDatacenterId(datacenterId);
    setWorkerId(workerId);
  }

  /**
   * Set data center id.
   *
   * @param datacenterId Data center number the process running on, value must is between 0 and 31.
   */
  public void setDatacenterId(long datacenterId) {
    if (datacenterId > maxDatacenterId || datacenterId < 0) {
      throw new IllegalArgumentException(
          String.format("datacenter id can't be greater than %d or less than 0", maxDatacenterId));
    }
    this.datacenterId = datacenterId;
  }

  /**
   * Set worker id.
   *
   * @param workerId Machine or process number, value must is between 0 and 31.
   */
  public void setWorkerId(long workerId) {
    if (workerId > maxWorkerId || workerId < 0) {
      throw new IllegalArgumentException(
          String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
    }
    this.workerId = workerId;
  }

  /**
   * Get snowflake id config.
   *
   * @return Snowflake ID config instance.
   */
  public IdConfig getConfig() {
    return new IdConfig(timestampBits, datacenterIdBits, workerIdBits, sequenceBits,
        new IdEntity(offsetTimestamp, datacenterId, workerId, sequence));
  }

  /** Synchronized lock. */
  private final Lock lock = new ReentrantLock();

  /**
   * Generate an unique and incrementing ID.
   *
   * @return Snowflake ID.
   */
  public long nextId() {
    try {
      lock.lock();
      if (datacenterId < 0 || workerId < 0) {
        throw new IllegalArgumentException("datacenter Id and worker Id MUST BE initialization.");
      }

      long currTimestamp = now();

      if (currTimestamp < lastTimestamp) {
        throw new IllegalStateException(
            String.format("Clock moved backwards. Refusing to generate id for %d milliseconds",
                lastTimestamp - currTimestamp));
      }

      if (currTimestamp == lastTimestamp) {
        sequence = (sequence + 1) & maxSequence;
        if (sequence == 0) { // overflow: greater than max sequence
          currTimestamp = waitNextMillis(currTimestamp);
        }
      } else { // reset to 0 for next period/millisecond
        sequence = 0L;
      }

      lastTimestamp = currTimestamp;

      long snowflake = ((currTimestamp - offsetTimestamp) << timestampShift) | // timestamp
          (datacenterId << datacenterIdShift) | // datacenter
          (workerId << workerIdShift) | // worker
          sequence; // sequence

      return snowflake;
    } finally {
      lock.unlock();
    }
  }

  /**
   * Track the amount of calling {@link #waitNextMillis(long)} method.
   */
  private final AtomicLong waitCount = new AtomicLong(0);

  /**
   * @return The amount of calling {@link #waitNextMillis(long)} method.
   */
  public long getWaitCount() {
    return this.waitCount.get();
  }

  /**
   * Running loop blocking until next millisecond.
   *
   * @param	currTimestamp	Current timestamp.
   * @return Current timestamp in millisecond.
   */
  private long waitNextMillis(long currTimestamp) {
    waitCount.incrementAndGet();
    while (currTimestamp <= lastTimestamp) {
      currTimestamp = now();
    }
    return currTimestamp;
  }

  /** Zone ID (UTC) */
  private static final ZoneId UTC = ZoneId.of("UTC");

  /**
   * Get now UTC timestamp millisecond.
   *
   * @return The UTC timestamp (millisecond).
   */
  private long now(){
    return Instant.now(Clock.systemUTC()).toEpochMilli();
  }

  /**
   * Formatter timestamp with ISO-8601 extended offset datetime format.
   *
   * @param timestamp Timestamp (millisecond).
   * @return ISO-8601 extended offset datetime format.
   */
  private String datetimeFormat(long timestamp) {
    ZonedDateTime datetime = Instant.ofEpochMilli(timestamp).atZone(UTC);
    return datetime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
  }

  /**
   * Parser snowflake id to entity.
   *
   * @param id Snowflake id.
   * @return {@link IdEntity} instance.
   */
  private IdEntity parserId(long id) {
    long timestamp = ((id & diode(unusedBits, timestampBits)) >> timestampShift) + offsetTimestamp;
    long datacenterId = (id & diode(unusedBits + timestampBits, datacenterIdBits)) >> datacenterIdShift;
    long workerId = (id & diode(unusedBits + timestampBits + datacenterIdBits, workerIdBits)) >> workerIdShift;
    long sequence = (id & diode(unusedBits + timestampBits + datacenterIdBits + workerIdBits, sequenceBits));
    return new IdEntity(timestamp, datacenterId, workerId, sequence);
  }

  /**
   * Formatter snowflake id.
   *
   * @param id Snowflake id.
   * @return ID formatting message.
   *   {@code 'ISO8601 format, #sequence id, @(datacenter id, worker id)'}
   */
  public String formatId(long id) {
    IdEntity entity = parserId(id);
    String tsf = datetimeFormat(entity.getTimestamp()); // time-stamp format
    return String.format("%s, #%d, @(%d,%d)", tsf, entity.getSequence(), entity.getDatacenterId(), entity.getWorkerId());
  }

  /**
   * A diode is a long value whose left and right margin are ZERO,
   * while middle bits are ONE in binary string layout.
   * It looks like a diode in shape.
   *
   * @param offset Left margin position.
   * @param length {@code 'offset + length'} is right margin position.
   * @return A long value.
   */
  private long diode(long offset, long length) {
    int lb = (int) (64 - offset);
    int rb = (int) (64 - (offset + length));
    return (-1L << lb) ^ (-1L << rb);
  }

  /**
   * Snowflake ID config string.
   *
   * @return Config string.
   */
  @Override
  public String toString() {
    return getConfig().toString();
  }
}
