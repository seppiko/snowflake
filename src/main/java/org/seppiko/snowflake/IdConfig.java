/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.snowflake;

/**
 * Snowflake ID config
 *
 * @author Leonard Woo
 */
public class IdConfig {

  private final long timestampBits;
  private final long datacenterIdBits;
  private final long workerIdBits;
  private final long sequenceBits;
  private final IdEntity entity;

  /**
   * Snowflake ID config constructor.
   *
   * @param timestampBits Datetime length.
   * @param datacenterIdBits Data center ID length.
   * @param workerIdBits Worker ID length.
   * @param sequenceBits Sequence length.
   * @param entity ID entity. See {@link IdEntity}.
   */
  protected IdConfig(long timestampBits, long datacenterIdBits, long workerIdBits, long sequenceBits,
      IdEntity entity) {
    this.timestampBits = timestampBits;
    this.datacenterIdBits = datacenterIdBits;
    this.workerIdBits = workerIdBits;
    this.sequenceBits = sequenceBits;
    this.entity = entity;
  }

  /**
   * @return Datetime length.
   */
  public long getTimestampBits() {
    return timestampBits;
  }

  /**
   * @return Data identifier id length.
   */
  public long getDatacenterIdBits() {
    return datacenterIdBits;
  }

  /**
   * @return Worker id length.
   */
  public long getWorkerIdBits() {
    return workerIdBits;
  }

  /**
   * @return Sequence length.
   */
  public long getSequenceBits() {
    return sequenceBits;
  }

  /**
   * @return ID config entity.
   */
  public IdEntity getEntity() {
    return entity;
  }

  /**
   * @return Snowflake id config string.
   */
  @Override
  public String toString() {
    return "Snowflake {" +
        "timestampBits=" + timestampBits +
        ", datacenterIdBits=" + datacenterIdBits +
        ", workerIdBits=" + workerIdBits +
        ", sequenceBits=" + sequenceBits +
        ", offsetTimestamp=" + entity.getTimestamp() +
        ", datacenterId=" + entity.getDatacenterId() +
        ", workerId=" + entity.getWorkerId() +
        ", sequence=" + entity.getSequence() +
        "}";
  }
}
