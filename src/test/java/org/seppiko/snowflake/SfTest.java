/*
 * Copyright 2022 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.snowflake;

import java.time.Clock;
import java.time.ZonedDateTime;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

/**
 * @author Leonard Woo
 */
public class SfTest {

  private static final Logger logger = LogManager.getLogger();

  @Test
  public void offsetTest() {
    logger.info("Offset Timestamp: " + ZonedDateTime.now(Clock.systemUTC()).toInstant().toEpochMilli());
  }

  @Test
  public void sfTest() {
    IdWorker sf = IdWorker.getInstance(1692929617446L);
    sf.initWorker(1L, 1L);
    for(int i = 0; i < 10; i++) {
      long id = sf.nextId();
      logger.info("ID: " + id + " " + Long.toHexString(id));
      logger.info("Formatter: " + sf.formatId(id));
    }
  }

}
